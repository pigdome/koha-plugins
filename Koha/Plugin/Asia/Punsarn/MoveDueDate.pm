package Koha::Plugin::Asia::Punsarn::MoveDueDate;

## It's good practive to use Modern::Perl
use Modern::Perl;

## Required for all plugins
use base qw(Koha::Plugins::Base);

## We will also need to include any Koha libraries we want to access
use C4::Context;
use C4::Branch;
use C4::Members;
use C4::Auth;
use Koha::DateUtils;

## Here we set our plugin version
our $VERSION = 1.02;

## Here is our metadata, some keys are required, some are optional
our $metadata = {
    name   			=> 'Change Dates Plugin',
    author 			=> 'Pigdome',
    description     => 'This plugin for change date expiry on all table by Pigdome',
    date_authored   => '2014-09-19',
    date_updated    => '2014-09-23',
    minimum_version => '3.0100107',
    maximum_version => undef,
    version         => $VERSION,
};

our $new_date;
our $old_date;



## This is the minimum code required for a plugin's 'new' method
## More can be added, but none should be removed
sub new {
    my ( $class, $args ) = @_;

    ## We need to add our metadata here so our base class can access it
    $args->{'metadata'} = $metadata;

    ## Here, we call the 'new' method for our base class
    ## This runs some additional magic and checking
    ## and returns our actual $self
    my $self = $class->SUPER::new($args);

    return $self;
}

## The existance of a 'report' subroutine means the plugin is capable
## of running a report. This example report can output a list of patrons
## either as HTML or as a CSV file. Technically, you could put all your code
## in the report method, but that would be a really poor way to write code
## for all but the simplest reports
sub tool{
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $next_step = $cgi->param('next_step');

    if ( $next_step eq 'step2' ) 
    {
        $self->tool_step2();
    }
    elsif( $next_step eq 'step3' )
    {
        $self->tool_step3();
    }
    else 
    {
        $self->tool_step1();
    }
}



## This is the 'install' method. Any database tables or other setup that should
## be done when the plugin if first installed should be executed in this method.
## The installation method should always return true if the installation succeeded
## or false if it failed.
sub install() {
    my ( $self, $args ) = @_;

    my $table = $self->get_qualified_table_name('mytable');

    return C4::Context->dbh->do( "
        CREATE TABLE  $table (
            `borrowernumber` INT( 11 ) NOT NULL
        ) ENGINE = INNODB;
    " );
}

## This method will be run just before the plugin files are deleted
## when a plugin is uninstalled. It is good practice to clean up
## after ourselves!
sub uninstall() {
    my ( $self, $args ) = @_;

    my $table = $self->get_qualified_table_name('mytable');

    return C4::Context->dbh->do("DROP TABLE $table");
}

## These are helper functions that are specific to this plugin
## You can manage the control flow of your plugin any
## way you wish, but I find this is a good approach
sub tool_step1 {

    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};
    my $template = $self->get_template({ file => 'tool-step1.tt' });

    print $cgi->header();
    print $template->output();
}

sub tool_step2 {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $dbh = C4::Context->dbh;
    $new_date = $cgi->param('new_date');
    $old_date = $cgi->param('old_date');
    
    my $t_new_date;
    my $t_old_date;
    if( $new_date && $old_date )
    {
    	$t_new_date = dt_from_string($new_date);
    	$t_old_date = dt_from_string($old_date);
    }
    my $t_old_date = substr($t_old_date,0,10);
    $t_old_date .= " 23:59:00";
    my $sql = "select * 
               from issues 
               where date_due = '$t_old_date'
                       ";

    

    my $sth = $dbh->prepare($sql);
    $sth->execute();
    
    my $role_count = 0;
    while ( my $row = $sth->fetchrow_hashref() ) 
    {
       $role_count++;
    }

    
    my $template = $self->get_template({ file => 'tool-step2.tt' });
 
    $template->param(
        role_count => $role_count,
        new_date => $new_date,
        old_date => $old_date,
    );

    
    print $cgi->header();
    print $template->output();
}

sub tool_step3 {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $dbh = C4::Context->dbh;
    my $confirm = $cgi->param('confirm');
    $new_date = $cgi->param('new_date');
    $old_date = $cgi->param('old_date');
    
    
    if( $new_date && $old_date )
    {
    	$new_date = dt_from_string($new_date);
    	$old_date = dt_from_string($old_date);
    }

    my $role_count = 0;
    my $t_old_date = substr($old_date,0,10);
    my $t_new_date = substr($new_date,0,10);

    $new_date = $t_new_date . " 23:59:00";
    $old_date = $t_old_date . " 23:59:00";
    if( $confirm )
    {
    	$role_count += $dbh->do(
            qq{
                UPDATE issues 
                SET date_due = ?
                WHERE date_due = ?
            },
            {},
            $new_date,
            $old_date,
        );
    }
    
    $dbh->do(
            qq{
                UPDATE items
                SET onloan = ?
                WHERE onloan = ?
            },
            {},
            $t_new_date,
            $t_old_date,
        );
    
    my $template = $self->get_template({ file => 'tool-step3.tt' });
 
    $template->param(
        role_count   => $role_count,
    );
    
    print $cgi->header();
    print $template->output();
}


1;
