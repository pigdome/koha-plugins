package Koha::Plugin::Asia::Punsarn::SplineLabel;

## It's good practive to use Modern::Perl
use Modern::Perl;

## Required for all plugins
use base qw(Koha::Plugins::Base);

## We will also need to include any Koha libraries we want to access
use C4::Context;
use C4::Branch;
use C4::Members;
use C4::Auth;
use C4::Debug;
use C4::Creators;

use C4::Labels::Batch;
use C4::Labels::Profile;
use C4::Labels::Template;
use C4::Labels::Layout;
use C4::Labels::Label;

use C4::Biblio;
use Koha::DateUtils;
use strict;
use warnings;
use CGI;
use Text::Wrap;
use Algorithm::CheckDigits;
use Text::CSV_XS;
use Data::Dumper;
use Library::CallNumber::LC;
use File::Copy;

## Here we set our plugin version
our $VERSION = 1.03;

## Here is our metadata, some keys are required, some are optional
our $metadata = {
    name   			=> 'Spline Label Plugin',
    author 			=> 'Pigdome',
    description     => 'This plugin for print lebel by Pigdome',
    date_authored   => '2014-11-26',
    date_updated    => '2014-11-26',
    minimum_version => '1.1',
    maximum_version => undef,
    version         => $VERSION,
};

our $new_date;
our $old_date;



## This is the minimum code required for a plugin's 'new' method
## More can be added, but none should be removed
sub new {
    my ( $class, $args ) = @_;

    ## We need to add our metadata here so our base class can access it
    $args->{'metadata'} = $metadata;
    my $source = "/var/lib/koha/demo/plugins/koha/PLugin/Asia/Punsarn/SplineLabel/Lebel_PI.pm";
    my $dest   = "//usr/share/koha/lib/C4/Labels/Label_PI.pm";
    copy($source,$dest) or die "File cannot be copied.";
    ## Here, we call the 'new' method for our base class
    ## This runs some additional magic and checking
    ## and returns our actual $self
    my $self = $class->SUPER::new($args);

    return $self;
}

## The existance of a 'report' subroutine means the plugin is capable
## of running a report. This example report can output a list of patrons
## either as HTML or as a CSV file. Technically, you could put all your code
## in the report method, but that would be a really poor way to write code
## for all but the simplest reports
sub tool{
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $next_step = $cgi->param('next_step');

    if ( $next_step eq 'step2' ) 
    {
        $self->tool_step2();
    }
    elsif( $next_step eq 'step3' )
    {
        $self->tool_step3();
    }
    else 
    {
        $self->tool_step1();
    }
}



## This is the 'install' method. Any database tables or other setup that should
## be done when the plugin if first installed should be executed in this method.
## The installation method should always return true if the installation succeeded
## or false if it failed.
sub install() {
    my ( $self, $args ) = @_;

    my $table = $self->get_qualified_table_name('mytable');

    return C4::Context->dbh->do( "
        CREATE TABLE  $table (
            `borrowernumber` INT( 11 ) NOT NULL
        ) ENGINE = INNODB;
    " );
}

## This method will be run just before the plugin files are deleted
## when a plugin is uninstalled. It is good practice to clean up
## after ourselves!
sub uninstall() {
    my ( $self, $args ) = @_;

    my $table = $self->get_qualified_table_name('mytable');

    return C4::Context->dbh->do("DROP TABLE $table");
}



## These are helper functions that are specific to this plugin
## You can manage the control flow of your plugin any
## way you wish, but I find this is a good approach

sub tool_step1 {

    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};
    my $template = $self->get_template({ file => 'tool-step1.tt' });

    print $cgi->header();
    print $template->output();
}

sub tool_step2 {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};
    my $dbh   = C4::Context->dbh;
    my $sql = "SELECT batch_id , COUNT(*) as Itemcount 
               FROM creator_batches GROUP BY batch_id ORDER BY Itemcount DESC";
    my $sth = $dbh->prepare($sql);
    $sth->execute();
    my $batchcount = $sth->fetchall_arrayref( {} );
        
    my $template = $self->get_template( { file => 'tool-step2.tt' } );

    $template->param(
        %$args,
        batchcount => $batchcount,
    );

    print $cgi->header();
    print $template->output();
}

sub tool_step3 {
    my ( $self, $args ) = @_;
    my $cgi_old = $self->{'cgi'};
    my $cgi = new CGI;

    my $batch_id    = $cgi_old->param('bid') if $cgi_old->param('bid');
    my $template_id = 26;
    my $layout_id   = 22;
    my $start_label =  1;


    my $pdf_file = "label_batch_$batch_id";
    print $cgi->header( -type       => 'application/pdf',
                        -encoding   => 'utf-8',
                        -attachment => "$pdf_file.pdf",
                      );

    our $pdf = C4::Creators::PDF->new(InitVars => 0) or die "pdf";
    my $batch = C4::Labels::Batch->retrieve(batch_id => $batch_id) or die "create batch";
    our $template = C4::Labels::Template->retrieve(template_id => $template_id, profile_id => 1) or die "create lebel";
    my $layout = C4::Labels::Layout->retrieve(layout_id => $layout_id) or die "create layout";

    # set the paper size
    my $lowerLeftX  = 0;
    my $lowerLeftY  = 0;
    my $upperRightX = $template->get_attr('page_width');
    my $upperRightY = $template->get_attr('page_height');

    $pdf->Compress(1);
    $pdf->Mbox($lowerLeftX, $lowerLeftY, $upperRightX, $upperRightY);

    my ($row_count, $col_count, $llx, $lly) = $template->get_label_position($start_label);
    my $items = $batch->get_attr('items');
    my $output = "";
    #LABEL_ITEMS:
    foreach my $item (@{$items}) 
    {
        my ($barcode_llx, $barcode_lly, $barcode_width, $barcode_y_scale_factor) = 0,0,0,0;
        
            my $label = C4::Labels::Label->new(
                                            batch_id            => $batch_id,
                                            item_number         => $item->{'item_number'},
                                            llx                 => $llx,
                                            lly                 => $lly,
                                            width               => $template->get_attr('label_width'),
                                            height              => $template->get_attr('label_height'),
                                            top_text_margin     => $template->get_attr('top_text_margin'),
                                            left_text_margin    => $template->get_attr('left_text_margin'),
                                            barcode_type        => $layout->get_attr('barcode_type'),
                                            printing_type       => $layout->get_attr('printing_type'),
                                            guidebox            => $layout->get_attr('guidebox'),
                                            font                => $layout->get_attr('font'),
                                            font_size           => $layout->get_attr('font_size'),
                                            callnum_split       => $layout->get_attr('callnum_split'),
                                            justify             => $layout->get_attr('text_justify'),
                                            format_string       => $layout->get_attr('format_string'),
                                            text_wrap_cols      => $layout->get_text_wrap_cols(label_width => $template->get_attr('label_width'), left_text_margin => $template->get_attr('left_text_margin')),
                                              );
            $pdf->Add($label->draw_guide_box) if $layout->get_attr('guidebox');
            my $label_text = $label->create_label();
            #---------------------------------------------------------------
            
            foreach my $text_line (@$label_text) 
            {
                $pdf->Font($text_line->{'font'});
                $pdf->FontSize( $text_line->{'font_size'} );
                $pdf->Text( $text_line->{'text_llx'}, $text_line->{'text_lly'}, $text_line->{'line'} );
                $output .= $text_line->{'line'} . "::";
            }
            #---------------------------------------------------------------------
            if ($col_count < $template->get_attr('cols')) {
                $llx = ($llx + $template->get_attr('label_width') + $template->get_attr('col_gap'));
                $col_count++;
            }
            else {
                $llx = $template->get_attr('left_margin');
                if ($row_count == $template->get_attr('rows')) {
                    $pdf->Page();
                    $lly = ($template->get_attr('page_height') - $template->get_attr('top_margin') - $template->get_attr('label_height'));
                    $row_count = 1;
                }
                else {
                    $lly = ($lly - $template->get_attr('row_gap') - $template->get_attr('label_height'));
                    $row_count++;
                }
                $col_count = 1;
            }
            #------------------------------------------------------------------------
            #next LABEL_ITEMS;
    }

    $pdf->End();
}

1;
