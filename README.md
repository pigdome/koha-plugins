# Koha : Plugins #

This is tutorail for create plugins on Koha

### 1. สร้าง folder ให้ตรงกับ package ใน file perl ###

package Koha::Plugin::Asia::Punsarn::MoveDueDate;

==> Koha/Plugins/Asia/Punsarn/Movedate.pm

### 2. สร้าง method บังคับของ Koha ###

* install
* uninstall
* new
* tool หรือ new 

### 3. สร้าง metadata ###
ตัวอย่าง
![Capture.PNG](https://bitbucket.org/repo/qoBgoq/images/1858486074-Capture.PNG)

### 4. Koha ใช้ template ในการนำข้อมูลมาแสดงผล ###

* สร้าง tag html ใน file .tt
* การเรียก template
* > 
 ```
   my $template = $self->get_template({ file => 'tool-step1.tt' });
 ```
 * >
```
   print $template->output();
 ```
* การส่งค่าไปยัง template
* >
  ```
         $template->param(
             x => $x,
             y => $y,
             z => $z,
          );
    ```
* การ get ค่ามาแสดงผลใน template 
* >
  ```
      <input type="text" id="x" name="x"  value="[% x %]" />
  ```